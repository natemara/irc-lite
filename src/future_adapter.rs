use bus::BusReader;
use futures::{Async, Poll, Stream};
use std::sync::mpsc::TryRecvError;

pub struct BusReaderAdapter<T>(BusReader<T>);

pub fn recv_future<T>(reader: BusReader<T>) -> BusReaderAdapter<T> {
    BusReaderAdapter(reader)
}

impl<T> Stream for BusReaderAdapter<T>
where
    T: Clone + Sync,
{
    type Item = T;
    type Error = ();

    fn poll(&mut self) -> Poll<Option<Self::Item>, Self::Error> {
        match self.0.try_recv() {
            Ok(val) => Ok(Async::Ready(Some(val))),
            Err(TryRecvError::Empty) => Ok(Async::NotReady),
            Err(TryRecvError::Disconnected) => Ok(Async::Ready(None)),
        }
    }
}
